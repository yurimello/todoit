# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
if User.count == 0
  users = User.create([
    {name: 'User', email: 'user@email.com', password: 'password123', password_confirmation: 'password123'},
    {name: 'User2', email: 'user2@email.com', password: 'password123', password_confirmation: 'password123'}
  ])
else
  users = User.all
end

Board.destroy_all
boards = Board.create([
  {name: 'own board', owner: users.first},
  {name: 'public board', owner: users.last},
  {name: 'private board', owner: users.last, is_private: true}
])

Step.destroy_all
steps = Step.create([
  {name: 'To Do', board: boards.first},
  {name: 'Doing', board: boards.first},
  {name: 'Done', board:  boards.first},
  {name: 'To Do', board: boards[1]},
  {name: 'To Do', board: boards.last}
])

Story.delete_all
stories = Story.create([
  {name: 'Create Repo', step: steps[0], owner: users.last},
  {name: 'Create heroku project', step: steps[0], owner: users.last, is_private: true},
  {name: 'Create models', step: steps[0], owner: users.first}
])

Task.delete_all
tasks = Task.create([
  { name: 'Implements Board model', story: stories.last },
  { name: 'Implements Step model', story: stories.last },
  { name: 'Implements Story model', story: stories.last },
  { name: 'Implements Task model', story: stories.last },
])
class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.string :name
      t.boolean :is_private
      t.integer :user_id
      t.references :step, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :stories, :user_id
  end
end

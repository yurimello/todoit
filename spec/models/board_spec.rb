require 'rails_helper'

RSpec.describe Board, type: :model do
  describe "associations" do
    it { should belong_to(:owner).class_name('User') }
    it { should have_many(:steps) }
  end

  describe "validations" do
    it { should validate_presence_of(:name) }
  end

  describe 'scopes' do
    subject { Board }
    
    describe '.accessible_for' do
      let(:user) { create(:user) }
      let(:another_user) { create(:user) }

      let!(:own_board) { create(:board, owner: user) }
      let!(:public_board) { create(:board, owner: another_user) }
      let!(:another_public_board) { create(:board, is_private: false, owner: another_user) }
      let!(:private_board) { create(:private_board, owner: another_user) }

      it { expect(subject.accessible_for(user.id).count).to eq(3) }
      it { expect(subject.accessible_for(user.id)).to include(public_board) }
      it { expect(subject.accessible_for(user.id)).to_not include(private_board) }
    end
  end
end

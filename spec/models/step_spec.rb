require 'rails_helper'

RSpec.describe Step, type: :model do
  describe "associations" do
    it { should belong_to(:board) }
    it { should have_many(:stories) }
  end

  describe "validations" do
    it { should validate_presence_of(:board) }
    it { should validate_presence_of(:name) }
  end
end
require 'rails_helper'

RSpec.describe Story, type: :model do
  describe "associations" do
    it { should belong_to(:owner).class_name('User') }
    it { should belong_to(:step) }
    it { should have_many(:tasks) }

    it 'destroys all tasks aftter destroy parent story' do
      story = create(:story)
      create(:task, story: story)
      expect{ story.destroy }.to change(Task, :count).by(-1)
    end
  end

  describe "validations" do
    it { should validate_presence_of(:name) }
  end

  describe 'scopes' do
    subject { Story }
    
    describe '.accessible_for' do
      let(:user) { create(:user) }
      let(:another_user) { create(:user) }

      let!(:own_story) { create(:story, owner: user) }
      let!(:public_story) { create(:story, owner: another_user) }
      let!(:another_public_story) { create(:story, is_private: false, owner: another_user) }
      let!(:private_story) { create(:private_story, owner: another_user) }

      it { expect(subject.accessible_for(user.id).count).to eq(3) }
      it { expect(subject.accessible_for(user.id)).to include(public_story) }
      it { expect(subject.accessible_for(user.id)).to_not include(private_story) }
    end
  end

  describe "ActiveRecord::Callbacks" do
    context 'when is new record' do
      subject { build(:story) }

      it 'changes position' do
        expect { subject.save }.to change { subject.position }.from(nil).to(0)
      end
    end

    context 'when is existent record' do
      subject { create(:story) }
      let!(:first_story) { create(:story, step: subject.step) }
      let!(:second_story) { create(:story, step: subject.step) }
      let!(:third_story) { create(:story, step: subject.step) }

      it 'does not change position' do
        expect { subject.save }.to_not change { subject.position }
      end

      context 'when updates position to 2' do
        before(:each) do
          subject.update_attributes(position: 2)
        end
        it { expect(subject.reload.position).to eq(2) }
        it { expect(first_story.reload.position).to eq(0) }
        it { expect(second_story.reload.position).to eq(1) }
        it { expect(third_story.reload.position).to eq(3) }
      end
    end
  end

  describe '#change_position' do
    let(:step) { create(:step) }
    let!(:first_story) { create(:story, step: step) }
    let!(:second_story) { create(:story, step: step) }
    let!(:third_story) { create(:story, step: step) }

    context 'when first story goes to third position' do
      before(:each) do
        first_story.change_position(2)
      end

      it { expect(first_story.reload.position).to eq(2) }
      it { expect(second_story.reload.position).to eq(0) }
      it { expect(third_story.reload.position).to eq(1) }
    end

    context 'when first story goes to second position' do
      before(:each) do
        first_story.change_position(1)
      end

      it { expect(first_story.reload.position).to eq(1) }
      it { expect(second_story.reload.position).to eq(0) }
      it { expect(third_story.reload.position).to eq(2) }
    end

    context 'when third story goes to first position' do
      before(:each) do
        third_story.change_position(0)
      end
      it { expect(first_story.reload.position).to eq(1) }
      it { expect(second_story.reload.position).to eq(2) }
      it { expect(third_story.reload.position).to eq(0) }
    end

    context 'when second story goes to first position' do
      before(:each) do
        second_story.change_position(0)
      end
      it { expect(first_story.reload.position).to eq(1) }
      it { expect(second_story.reload.position).to eq(0) }
      it { expect(third_story.reload.position).to eq(2) }
    end

    context 'when second story goes to third position' do
      before(:each) do
        second_story.change_position(2)
      end
      it { expect(first_story.reload.position).to eq(0) }
      it { expect(second_story.reload.position).to eq(2) }
      it { expect(third_story.reload.position).to eq(1) }
    end

     context 'when third story goes to second position' do
      before(:each) do
        third_story.change_position(1)
      end
      it { expect(first_story.reload.position).to eq(0) }
      it { expect(second_story.reload.position).to eq(2) }
      it { expect(third_story.reload.position).to eq(1) }
    end

  end
end

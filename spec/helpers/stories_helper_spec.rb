require 'rails_helper'
RSpec.describe StoriesHelper, type: :helper do
  let(:user)   { create(:user) }
  let!(:story) { create(:story, owner: user) }
  
  before(:each) do
    login_as(user, :scope => :user)
    allow(helper).to receive(:current_user) { user }
    @board = create(:board)
  end

  describe '#edit_story_button' do
    context 'with own story' do
      it 'returns a link' do
        @story = story
        expect(helper.edit_story_button).to_not eq(nil)
      end
    end

    context 'with another user story' do
      let(:another_user) { create(:user) }
      let(:another_story) { create(:story, owner: another_user) }
      it 'does not returns a link' do
        @story = another_story
        expect(helper.edit_story_button).to eq(nil)
      end
    end
  end

  describe '#delete_story_button' do
    context 'with own story' do
      it 'returns a link' do
        link = helper.delete_story_button(story)
        expect(link).to_not eq(nil)
      end
    end

    context 'with another user story' do
      let(:another_user) { create(:user) }
      let(:another_story) { create(:story, owner: another_user) }
      it 'does not returns a link' do
        link = helper.delete_story_button(another_story)
        expect(link).to eq(nil)
      end
    end
  end
end
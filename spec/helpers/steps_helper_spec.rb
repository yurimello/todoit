require 'rails_helper'
RSpec.describe StepsHelper, type: :helper do
  let(:user)   { create(:user) }
  let(:board)  { create(:board) }
  let(:step)   { create(:step, board: board) }
  
  before(:each) do
    login_as(user, :scope => :user)
    allow(helper).to receive(:current_user) { user }
  end

  describe '#delete_step_button' do
    it 'returns a link' do
      @board = board
      link = helper.delete_step_button(step)
      expect(link).to_not eq(nil)
    end
  end
end
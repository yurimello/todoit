require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the BoardsHelper. For example:
#
# describe BoardsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe BoardsHelper, type: :helper do
  let(:user) { create(:user) }
  let!(:board) { create(:board, owner: user) }
  
  before(:each) do
    login_as(user, :scope => :user)
    allow(helper).to receive(:current_user) { user }
  end
  describe '#table_board_for_user' do
    context 'with own board' do
      it 'sees success class' do
        table = helper.table_board_for_user(board) { }
        expect(table).to have_selector('.success')
      end
    end
    
    context 'with another user board' do
      let(:another_user) { create(:user) }
      let(:another_board) { create(:board, owner: another_user) }
      it 'does not success class' do
        table = helper.table_board_for_user(another_board) { }
        expect(table).to_not have_selector('.success')
      end
    end
  end

end

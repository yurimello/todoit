require 'rails_helper'
RSpec.describe TasksHelper, type: :helper do
  let(:user)   { create(:user) }
  let(:story)  { create(:story, owner: user) }
  let(:task)   { create(:task, story: story) }
  
  before(:each) do
    login_as(user, :scope => :user)
    @board = story.step.board
    @story = story
    
    allow(helper).to receive(:current_user) { user }
  end

  describe '#edit_task_button' do
    context 'with own story' do
      it 'returns a link' do
        link = helper.edit_task_button(task)
        expect(link).to_not eq(nil)
      end
    end

    context 'with another user story' do
      let(:another_user) { create(:user) }
      let(:another_story) { create(:story, owner: another_user) }
      let(:another_task)   { create(:task, story: another_story) }
      it 'does not returns a link' do
        link = helper.edit_task_button(another_task)
        expect(link).to eq(nil)
      end
    end
  end

  describe '#delete_task_button' do
    context 'with own story' do
      it 'returns a link' do
        link = helper.delete_task_button(task)
        expect(link).to_not eq(nil)
      end
    end

    context 'with another user story' do
      let(:another_user) { create(:user) }
      let(:another_story) { create(:story, owner: another_user) }
      let(:another_task)   { create(:task, story: another_story) }
      it 'does not returns a link' do
        link = helper.delete_task_button(another_task)
        expect(link).to eq(nil)
      end
    end
  end

  describe '#new_task_button' do
    context 'with own story' do
      it 'returns a link' do
        link = helper.new_task_button
        expect(link).to_not eq(nil)
      end
    end
    
    context 'with another user story' do
      let(:another_user) { create(:user) }
      let(:another_story) { create(:story, owner: another_user) }
      it 'does not returns a link' do
        @story = another_story
        link = helper.new_task_button
        expect(link).to eq(nil)
      end
    end
  end

end
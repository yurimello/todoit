FactoryGirl.define do
  sequence :board_name do |n|
    "board#{n}"
  end
  factory :board do
    name { generate(:board_name) }
    
    is_private nil

    factory :board_with_steps do
      after(:create) do |board, evaluator|
        create(:step_to_do, board: board) 
        create(:step_doing, board: board)
        create(:step_done,  board: board) 
      end
    end

    factory :private_board do
      name 'private board'
      is_private true
    end

    factory :invalid_board do
      name ''
    end
  end
end

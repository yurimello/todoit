FactoryGirl.define do
  sequence :story_name do |n|
    "story#{n}"
  end
  factory :story do
    name { generate(:story_name) }
    is_private false
    step
    
    factory :invalid_story do
      name ''
    end

    factory :private_story do
      name 'private story'
      is_private true
    end
  end


end

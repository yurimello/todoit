FactoryGirl.define do
  sequence :email do |n|
    "person#{n}@example.com"
  end

  factory :user do
    email                 
    name                  'Test User'
    password              'testpass'
    password_confirmation 'testpass'
  end

end

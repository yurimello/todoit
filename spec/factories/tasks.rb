FactoryGirl.define do
  factory :task do
    name 'task1'
    story
    done false
    factory :invalid_task do
      name ''
    end
  end

end

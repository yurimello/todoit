FactoryGirl.define do
  factory :step do
    board 
    name 'step1'
    
    factory :step_to_do do
      name 'To Do'
    end

    factory :step_doing do
      name "Doing"
    end

    factory :step_done do
      name "Done"
    end

    factory :step_without_board do
      name 'Without Board'
      board nil
    end

    factory :invalid_step do
      name ''
    end
  end
end

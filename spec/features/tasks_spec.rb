require "rails_helper"

RSpec.feature "Tasks", :type => :feature, js: true do
  given(:user)      { create(:user) }
  given(:board)     { create(:board, owner: user) }
  given(:step) { create(:step, board: board) }
  given(:story)     { create(:story, step: step, owner: user) }
  given!(:task)      { create(:task, story: story) }
  
  background do
    login_as(user, :scope => :user)
    visit "/boards"
    click_link board.name
  end
  
  describe 'Own story' do
    background do
      click_link story.name
      @task_list = find("#tasks-list")
    end
  
    describe 'Show tasks' do
      scenario 'User must see task' do
        expect(@task_list).to have_content(task.name)
      end

      scenario 'User must see edit button' do
        expect(@task_list).to have_selector('.edit-task-button')
      end

      scenario 'User must see delete button' do
        expect(@task_list).to have_selector('.delete-task-button')
      end
    end

    describe 'User deletes a task' do
      background do
        task_element = @task_list.find("#task_#{task.id}")
        page.accept_confirm do
          task_element.find('.delete-task-button').click
        end
      end

      scenario 'Task must desapear' do
        expect(@task_list).to_not have_content(task.name)
      end

      scenario 'User will not be redirected' do
        expect(page.current_path).to eq(board_path(board))
      end
    end

    describe 'New Task' do
      before do 
        click_link 'New Task'
      end

      describe 'User creates task' do
        before do
          fill_in :task_name, with: 'new task'
          click_button 'Save'
        end

        scenario 'New Task must shown in tasks list' do
          expect(@task_list).to have_content('new task')
        end

        scenario 'Edit new task must show edit form' do
          task_element = find('.list-group-item', text: 'new task')
          task_element.find('.edit-task-button').click
          expect(task_element).to have_selector('form')
        end
      end

      describe 'User cancel task creation' do
        before do
          click_link 'Cancel'
        end

        scenario 'User will not be redirected' do
          expect(page.current_path).to eq(board_path(board))
        end

        scenario 'User must see New Task button' do
          expect(page).to have_content("New Task")
        end
      end 

      describe 'With invalid data' do
        before do
          click_button 'Save'
        end

        scenario 'Name cant be blank error' do
          expect(page).to have_content(I18n.t('errors.messages.blank'))
        end
      end
    end

    describe 'Edit Task' do
      background do
        @task_element = @task_list.find("#task_#{task.id}")
        @task_element.find('.edit-task-button').click
      end

      scenario 'Edit button must desapear' do
        expect(@task_element).to_not have_selector('.edit-task-button')
      end

      scenario 'Delete button must desapear' do
        expect(@task_element).to_not have_selector('.delete-task-button')
      end

      scenario 'User must see Save button' do
        expect(@task_element).to have_selector('.save-task-button')
      end

      scenario 'User must see Cancel button' do
        expect(@task_list).to have_selector('.cancel-edit-task-button')
      end

      scenario 'User click cancel and sees edit button again' do
        @task_element.find('.cancel-edit-task-button').click
        expect(@task_element).to have_selector('.edit-task-button')
      end
     
      describe 'User updates task' do
        scenario 'task name must be updated' do
          @task_element.fill_in :task_name, with: 'new task name'
          @task_element.find('.save-task-button').click
          expect(@task_element).to have_content("new task name")
        end
      end

      describe 'With invalid data' do
        before do
          @task_element.fill_in :task_name, with: ''
          @task_element.find('.save-task-button').click
        end

        scenario 'Name cant be blank error' do
          expect(page).to have_content(I18n.t('errors.messages.blank'))
        end
      end

    end
  end
  
end
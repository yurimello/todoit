require "rails_helper"

RSpec.feature "Stories", :type => :feature, js: true do
  given(:user)      { create(:user) }
  given(:board)     { create(:board, owner: user) }
  given(:step) { create(:step, board: board) }
  given!(:story)    { create(:story, step: step, owner: user) }
  
  background do
    login_as(user, :scope => :user)
    visit "/boards"
  end
  
  describe 'Own story' do
    
    background do
      click_link board.name
    end
    scenario "User finds a story" do
      expect(page).to have_content(story.name)
    end

    scenario "User access a story" do
      click_link story.name
      expect(page).to have_content(story.name)
    end

    describe "User creates a story" do
      background do
        find('.new-story-button').click
      end
      
      scenario 'User does create story' do
        fill_in "story_name", with: 'test-story'
        find('.save-story-button').click
        expect(page).to have_content('test-story')
      end

      describe 'User does not fill story name' do
        background do
          find('.save-story-button').click
        end

        scenario 'User sees error message' do
          expect(page).to have_content(I18n.t('errors.messages.blank'))
        end
      end

      describe 'User gives up to create story' do
        background do
          fill_in "story_name", with: 'test-story'
          find('.cancel-form-story-button').click
        end

        scenario 'User does not create story' do
          expect(page).to_not have_content('test-story')
        end

        scenario 'User will be not redirected to board page' do
          expect(page.current_path).to eq(board_path(board))
        end
      end
    end

    describe "User edit a story" do
      background do
        click_link story.name
        find('.edit-story-button').click
        fill_in "story_name", with: 'test-story'
      end
      
      scenario 'User does update story' do
        find('.save-story-button').click
        expect(page).to have_content('test-story')
      end

      describe 'User gives up to update a story' do
        background do
          find('.cancel-form-story-button').click
        end

        scenario 'User does not update story' do
          expect(page).to have_content(story.name)
        end
      end
      
      describe 'User does not fill story name' do
        scenario 'User sees error message' do
          fill_in "story_name", with: ''
          find('.save-story-button').click
          expect(page).to have_content(I18n.t('errors.messages.blank'))
        end
      end
    end
    

    describe 'User deletes a story' do
      background do
        page.accept_confirm do
          find('.delete-story-button').click
        end
      end
      scenario 'Story will disappear from page' do
        expect(page).to_not have_content(story.name)
      end

      scenario 'User will be not redirected' do
        expect(page.current_path).to eq(board_path(board))
      end
    end
  end

  describe 'Another user public story' do
    given(:another_user)      { create(:user) }
    given!(:another_story)    { create(:story, step: step, owner: another_user) }

    background do
      click_link board.name
    end

    scenario "User finds a another user story" do
      expect(page).to have_content(another_story.name)
    end

    scenario "User cant see delete button for another story" do
      element = find("#story_#{another_story.id}")
      expect(element).to_not have_selector('.delete-story-button')
    end

    scenario "User cant see edit button for another story" do
      click_link another_story.name
      expect(page).to_not have_selector('.edit-story-button')
    end

    scenario "User cant see new task button for another story" do
      click_link another_story.name
      expect(page).to_not have_selector('.new-task-button')
    end
  end
end
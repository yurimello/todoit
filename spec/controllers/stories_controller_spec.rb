require 'rails_helper'

RSpec.describe StoriesController, type: :controller do
  let(:user)       { create(:user) }
  let!(:story)     { create(:story, owner: user) }
  let(:step)  { story.step } 
  let(:board)      { step.board }
  
  before(:each) do
    login_with(user)
  end

  let(:valid_attributes) {
    attributes_for(:story, step_id: step.id)
  }

  let(:invalid_attributes) {
    attributes_for(:invalid_story)
  }


  describe "GET #new" do
    before(:each) do
      get :new, { board_id: board.id, step_id: step.id }
    end

    it { expect(assigns(:story)).to be_a_new(story.class) } 

    it { expect(assigns(:board)).to be_a(board.class) }
  end

  describe "GET #edit" do
    it "assigns the requested story as @story" do
      get :edit, {board_id: board.id, step_id: step.id, id: story.to_param}
      expect(assigns(:story)).to eq(story)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      let(:params) { {board_id: board.id, step_id: step.id, story: valid_attributes} }

      it "creates a new Story" do
        expect {
          post :create, params
        }.to change(Story, :count).by(1)
      end

      it "assigns a newly created story as @story" do
        post :create, params
        expect(assigns(:story)).to be_a(Story)
        expect(assigns(:story)).to be_persisted
      end

      it "redirects to the created story" do
        post :create, params
        expect(response).to redirect_to(Story.last.step.board)
      end
    end

    context "with invalid params" do
      let(:params) { {board_id: board.id, step_id: step.id, story: invalid_attributes} }
      it "assigns a newly created but unsaved story as @story" do
        post :create, params
        expect(assigns(:story)).to be_a_new(Story)
      end

      it "re-renders the 'new' template" do
        post :create, params
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {name: 'new name'}
      }

      let(:params) {
        {:id => story.to_param, step_id: step.id, board_id: board.id, story: new_attributes} 
      }

      it "updates the requested story" do
        old_name = story.name
        expect { put :update, params }.
        to change { story.reload.name }.from(old_name).to(new_attributes[:name])
      end

      it "assigns the requested story as @story" do
        put :update, params
        expect(assigns(:story)).to eq(story)
      end

      it "redirects to the story" do
        put :update, params
        expect(response).to redirect_to(story.step.board)
      end
    end

    context "with invalid params" do
      let(:params) {
        {:id => story.to_param, step_id: step.id, board_id: board.id, story: invalid_attributes} 
      }

      it "assigns the story as @story" do
        put :update, params
        expect(assigns(:story)).to eq(story)
      end

      it "re-renders the 'edit' template" do
        put :update, params
        expect(response).to render_template("edit")
      end
    end
  end

  # describe "DELETE #destroy" do
  #   let(:another_user) { create(:user) }
      
  #   let(:private_step) { create(:private_step, owner: another_user) }

  #   it "destroys the requested step" do
  #     expect {
  #       delete :destroy, {:id => step.to_param}
  #     }.to change(step.class, :count).by(-1)
  #   end

  #   it "redirects to the steps list" do
  #     delete :destroy, {:id => step.to_param}
  #     expect(response).to redirect_to(steps_url)
  #   end

  #   context 'when access a private step from another user' do
  #     it 'redirects to steps index' do
  #       delete :destroy, {:id => private_step.to_param}
  #       expect(response).to redirect_to(steps_url)
  #     end
  #   end

  # end

end

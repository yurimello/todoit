require 'rails_helper'

RSpec.describe BoardsController, type: :controller do
  let(:user)  { create(:user) }
  
  
  let!(:board) { create(:board, owner: user) } 
  
  before(:each) do
    login_with(user)
  end

  let(:valid_attributes) {
    attributes_for(:board)
  }

  let(:invalid_attributes) {
    attributes_for(:invalid_board)
  }

  describe "GET #index" do

    it "assigns all boards as @boards" do
      get :index, {}
      expect(assigns(:boards)).to eq([board])
    end

    context 'with own boards and another users boards' do
      let(:another_user) { create(:user) }
      
      let!(:public_board) { create(:board, owner: another_user) }
      let!(:private_board) { create(:private_board, owner: another_user) }
     
      before(:each) do 
        get :index
        @boards = assigns(:boards)
      end

      it 'could not sees another user private board' do
        expect(@boards).to_not include(private_board)
      end

      it 'could sees public_board' do
        expect(@boards).to include(public_board)
      end

      it 'could sees own board' do
        expect(@boards).to include(board)
      end
    end
  end

  describe "GET #show" do
    let(:another_user) { create(:user) }
      
    let(:private_board) { create(:private_board, owner: another_user) }
    let(:public_board) { create(:board, owner: another_user) }

    context 'when access own board' do
      it "assigns the requested board as @board" do
        get :show, {:id => board.to_param}
        expect(assigns(:board)).to eq(board)
      end
    end
    
    context 'when access a private board from another user' do
      it 'redirects to boards index' do
        get :show, {:id => private_board.to_param}
        expect(response).to redirect_to(boards_url)
      end

      it 'renders error message' do
        get :show, {:id => private_board.to_param}
        expect(flash[:notice]).to eq(I18n.t('boards.show.errors_messages.access_denied'))
      end
    end
    
    context 'when access a public board from another user' do
      before(:each) do
        get :show, {:id => public_board.to_param}
      end

      it 'renders page' do
        expect(response).to render_template(:show)
      end

      it 'returns 200 status code' do
        expect(response).to have_http_status(:ok)
      end
    end
  end

  describe "GET #new" do
    it "assigns a new board as @board" do
      get :new, {}
      expect(assigns(:board)).to be_a_new(board.class)
    end
  end

  describe "GET #edit" do
    let(:another_user) { create(:user) }
      
    let(:private_board) { create(:private_board, owner: another_user) }

    it "assigns the requested board as @board" do
      get :edit, {:id => board.to_param}
      expect(assigns(:board)).to eq(board)
    end

    context 'when access a private board from another user' do
      it 'redirects to boards index' do
        get :edit, {:id => private_board.to_param}
        expect(response).to redirect_to(boards_url)
      end
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Board" do
        expect {
          post :create, {:board => valid_attributes}
        }.to change(Board, :count).by(1)
      end

      it "assigns a newly created board as @board" do
        post :create, {:board => valid_attributes}
        expect(assigns(:board)).to be_a(Board)
        expect(assigns(:board)).to be_persisted
      end

      it "redirects to the created board" do
        post :create, {:board => valid_attributes}
        expect(response).to redirect_to(Board.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved board as @board" do
        post :create, {:board => invalid_attributes}
        expect(assigns(:board)).to be_a_new(Board)
      end

      it "re-renders the 'new' template" do
        post :create, {:board => invalid_attributes}
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    let(:another_user) { create(:user) }
      
    let(:private_board) { create(:private_board, owner: another_user) }

    context "with valid params" do
      let(:new_attributes) {
        {name: 'new name'}
      }

      it "updates the requested board" do
        old_name = board.name
        expect { put :update, {:id => board.to_param, :board => new_attributes} }.
        to change { board.reload.name }.from(old_name).to(new_attributes[:name])
      end

      it "assigns the requested board as @board" do
        put :update, {:id => board.to_param, :board => valid_attributes}
        expect(assigns(:board)).to eq(board)
      end

      it "redirects to the board" do
        put :update, {:id => board.to_param, :board => valid_attributes}
        expect(response).to redirect_to(board)
      end
    end

    context "with invalid params" do
      it "assigns the board as @board" do
        put :update, {:id => board.to_param, :board => invalid_attributes}
        expect(assigns(:board)).to eq(board)
      end

      it "re-renders the 'edit' template" do
        put :update, {:id => board.to_param, :board => invalid_attributes}
        expect(response).to render_template("edit")
      end
    end

    context 'when access a private board from another user' do
      it 'redirects to boards index' do
        put :update, {:id => private_board.to_param, :board => valid_attributes}
        expect(response).to redirect_to(boards_url)
      end
    end
  end

  describe "DELETE #destroy" do
    let(:another_user) { create(:user) }
      
    let(:private_board) { create(:private_board, owner: another_user) }

    it "destroys the requested board" do
      expect {
        delete :destroy, {:id => board.to_param}
      }.to change(board.class, :count).by(-1)
    end

    it "redirects to the boards list" do
      delete :destroy, {:id => board.to_param}
      expect(response).to redirect_to(boards_url)
    end

    context 'when access a private board from another user' do
      it 'redirects to boards index' do
        delete :destroy, {:id => private_board.to_param}
        expect(response).to redirect_to(boards_url)
      end
    end

  end

end

require 'rails_helper'

RSpec.describe StepsController, type: :controller do
  let(:user)  { create(:user) }
  let!(:step) { create(:step) } 
  let(:board) { step.board }
  
  before(:each) do
    login_with(user)
  end

  let(:valid_attributes) {
    attributes_for(:step, board_id: board.id)
  }

  let(:invalid_attributes) {
    attributes_for(:invalid_step)
  }


  describe "GET #new" do
    before(:each) do
      get :new, { board_id: board.id }
    end

    it { expect(assigns(:step)).to be_a_new(step.class) } 

    it { expect(assigns(:board)).to be_a(board.class) }
  end

  describe "GET #edit" do
    it "assigns the requested step as @step" do
      get :edit, {board_id: board.id, id: step.to_param}
      expect(assigns(:step)).to eq(step)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      let(:params) { {board_id: board.id, step: valid_attributes} }

      it "creates a new Step" do
        expect {
          post :create, params
        }.to change(Step, :count).by(1)
      end

      it "assigns a newly created step as @step" do
        post :create, params
        expect(assigns(:step)).to be_a(Step)
        expect(assigns(:step)).to be_persisted
      end

      it "redirects to the created step" do
        post :create, params
        expect(response).to redirect_to(Step.last.board)
      end
    end

    context "with invalid params" do
      let(:params) { {board_id: board.id, step: invalid_attributes} }
      it "assigns a newly created but unsaved step as @step" do
        post :create, params
        expect(assigns(:step)).to be_a_new(Step)
      end

      it "re-renders the 'new' template" do
        post :create, params
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {name: 'new name'}
      }

      let(:params) {
        {:id => step.to_param, :step => new_attributes, board_id: board.id} 
      }

      it "updates the requested step" do
        old_name = step.name
        expect { put :update, params }.
        to change { step.reload.name }.from(old_name).to(new_attributes[:name])
      end

      it "assigns the requested step as @step" do
        put :update, params
        expect(assigns(:step)).to eq(step)
      end

      it "redirects to the step" do
        put :update, params
        expect(response).to redirect_to(step.board)
      end
    end

    context "with invalid params" do
      let(:params) {
        {:id => step.to_param, :step => invalid_attributes, board_id: board.id} 
      }

      it "assigns the step as @step" do
        put :update, params
        expect(assigns(:step)).to eq(step)
      end

      it "re-renders the 'edit' template" do
        put :update, params
        expect(response).to render_template("edit")
      end
    end
  end

  # describe "DELETE #destroy" do
  #   let(:another_user) { create(:user) }
      
  #   let(:private_step) { create(:private_step, owner: another_user) }

  #   it "destroys the requested step" do
  #     expect {
  #       delete :destroy, {:id => step.to_param}
  #     }.to change(step.class, :count).by(-1)
  #   end

  #   it "redirects to the steps list" do
  #     delete :destroy, {:id => step.to_param}
  #     expect(response).to redirect_to(steps_url)
  #   end

  #   context 'when access a private step from another user' do
  #     it 'redirects to steps index' do
  #       delete :destroy, {:id => private_step.to_param}
  #       expect(response).to redirect_to(steps_url)
  #     end
  #   end

  # end

end

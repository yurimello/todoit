require 'rails_helper'

RSpec.describe TasksController, type: :controller do
  let(:user)       { create(:user) }
  let(:story)      { create(:story, owner: user) }
  let!(:task)      { create(:task, story: story) }
  let(:step)  { story.step }
  let(:board)      { step.board }
  
  before(:each) do
    login_with(user)
  end

  let(:valid_attributes) {
    attributes_for(:task, story_id: story.id)
  }

  let(:invalid_attributes) {
    attributes_for(:invalid_task)
  }


  describe "GET #new" do
    before(:each) do
      get :new, { board_id: board.id, story_id: story.id }
    end

    it { expect(assigns(:task)).to be_a_new(task.class) } 

    it { expect(assigns(:board)).to be_a(board.class) }
  end

  # describe "GET #edit" do
  #   it "assigns the requested task as @task" do
  #     get :edit, {board_id: board.id, story_id: story.id, id: task.to_param}
  #     expect(assigns(:task)).to eq(task)
  #   end
  # end

  describe "POST #create" do
    context "with valid params" do
      let(:params) { {board_id: board.id, story_id: story.id, task: valid_attributes} }

      it "creates a new Task" do
        expect {
          post :create, params
        }.to change(Task, :count).by(1)
      end

      it "assigns a newly created task as @task" do
        post :create, params
        expect(assigns(:task)).to be_a(Task)
        expect(assigns(:task)).to be_persisted
      end

      it "redirects to the created task" do
        post :create, params
        expect(response).to redirect_to(board)
      end
    end

    context "with invalid params" do
      let(:params) { {board_id: board.id, story_id: story.id, task: invalid_attributes} }
      it "assigns a newly created but unsaved task as @task" do
        post :create, params
        expect(assigns(:task)).to be_a_new(Task)
      end

      it "re-renders the 'new' template" do
        post :create, params
        expect(response).to render_template("new")
      end
    end
  end

  # describe "PUT #update" do
  #   context "with valid params" do
  #     let(:new_attributes) {
  #       {name: 'new name'}
  #     }

  #     let(:params) {
  #       {:id => task.to_param, story_id: story.id, board_id: board.id, task: new_attributes} 
  #     }

  #     it "updates the requested task" do
  #       old_name = task.name
  #       expect { put :update, params }.
  #       to change { task.reload.name }.from(old_name).to(new_attributes[:name])
  #     end

  #     it "assigns the requested task as @task" do
  #       put :update, params
  #       expect(assigns(:task)).to eq(task)
  #     end

  #     it "redirects to the task" do
  #       put :update, params
  #       expect(response).to redirect_to(board)
  #     end
  #   end

  #   context "with invalid params" do
  #     let(:params) {
  #       {:id => task.to_param, story_id: story.id, board_id: board.id, task: invalid_attributes} 
  #     }

  #     it "assigns the task as @task" do
  #       put :update, params
  #       expect(assigns(:task)).to eq(task)
  #     end

  #     it "re-renders the 'edit' template" do
  #       put :update, params
  #       expect(response).to render_template("edit")
  #     end
  #   end
  # end

  # describe "DELETE #destroy" do
  #   let(:another_user) { create(:user) }
      
  #   let(:private_task) { create(:private_task, owner: another_user) }

  #   it "destroys the requested task" do
  #     expect {
  #       delete :destroy, {:id => task.to_param}
  #     }.to change(task.class, :count).by(-1)
  #   end

  #   it "redirects to the tasks list" do
  #     delete :destroy, {:id => task.to_param}
  #     expect(response).to redirect_to(tasks_url)
  #   end

  #   context 'when access a private task from another user' do
  #     it 'redirects to tasks index' do
  #       delete :destroy, {:id => private_task.to_param}
  #       expect(response).to redirect_to(tasks_url)
  #     end
  #   end

  # end

end

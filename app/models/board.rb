class Board < ActiveRecord::Base
  belongs_to :owner, class_name: 'User', foreign_key: 'user_id'
  has_many   :steps, dependent: :destroy

  validates :name, presence: true

  scope :accessible_for, -> (user_id) do
    where([ "boards.user_id = :user_id OR boards.is_private IS NOT TRUE", user_id: user_id ])
  end
end

class Step < ActiveRecord::Base
  belongs_to :board
  has_many   :stories, -> { order(:position) }, dependent: :destroy

  validates :name, :board, presence: true 

end

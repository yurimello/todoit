class Story < ActiveRecord::Base
  belongs_to :owner, class_name: 'User', foreign_key: 'user_id'
  belongs_to :step
  
  has_many :tasks, dependent: :destroy

  validates :name, presence: true

  scope :accessible_for, -> (user_id) do
    where([ "stories.user_id = :user_id OR stories.is_private IS NOT TRUE", user_id: user_id ])
  end

  before_create :set_position
  before_update :update_positions

  def change_position(new_position, position = nil)
    position ||= self.position
    if new_position < position
      move_to_up(new_position)
    elsif new_position > position
      move_to_down(new_position)
    end
  end

  private
  def move_to_up(new_position)
    stories_from_top = Story.where([
      "stories.position >= ? AND stories.id != ? AND step_id = ?", 
      new_position, self.id, self.step_id
    ])
    stories_count = self.step.stories.count - 1
    stories_from_top.each do |s|
      next_postion = s.position + 1
      next_postion = next_postion > stories_count ? stories_count : next_postion 
      s.update_column(:position, next_postion)
    end
    update_column(:position, new_position)
  end

  def move_to_down(new_position)
    stories_from_bottom = Story.where([
      "stories.position <= ? AND stories.id != ? AND step_id = ?", 
      new_position, self.id, self.step_id
    ])

    stories_from_bottom.each do |s|
      previous_postion = s.position - 1
      previous_postion = previous_postion < 0 ? 0 : previous_postion 
      s.update_column(:position, previous_postion)
    end
    update_column(:position, new_position)
  end
  
  # ActiveRecord::Callbacks
  def update_positions

    if self.position_changed?
      self.change_position(position, position_was)
    end
  end

  def set_position
    self.position = step.stories.count
  end
end

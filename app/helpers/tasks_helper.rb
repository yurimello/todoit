module TasksHelper
  def edit_task_button(task)
    if task.story.owner == current_user
      link_to edit_board_story_task_path(@board, @story, task), 
      title: 'Edit Task',
      remote: true, class: 'edit-task-button' do 
        content_tag(:span, '', class: 'glyphicon glyphicon-pencil')
      end
    end
  end

  def new_task_button
    if @story.owner == current_user
      link_to t('tasks.list.actions.new'), 
      new_board_story_task_path(@board, @story), 
      title: 'New Task', remote: true, 
      class: 'btn btn-success new-task-button'
    end
  end

  def delete_task_button(task)
    if task.story.owner == current_user
      link_to board_story_task_path(@board, @story, task), method: :delete,
      title: 'Delete Task',
      data: { confirm: 'Are you sure?' },  
      remote: true, class: 'delete-task-button' do
        content_tag(:span, '', class: 'glyphicon glyphicon-remove')
      end
    end
  end
end
module StepsHelper
  def delete_step_button(step)
    path = board_step_path(@board, step)
    link_to( path, method: :delete, title: 'Delete StepFlow',
      data: { confirm: 'Are you sure?' }, 
      class: 'list-actions') do
      content_tag(:span, '', class: 'glyphicon glyphicon-remove')
    end
  end
end
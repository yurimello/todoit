module BoardsHelper
  def table_board_for_user(board, &block)
    content_class = board.owner == current_user ? 'success' : ''
    content_tag(:tr, class: content_class) do
      yield
    end
  end
end

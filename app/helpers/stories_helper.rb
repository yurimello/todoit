module StoriesHelper
  def edit_story_button
    if @story.owner == current_user
      link_to t('stories.show.actions.edit'), 
      edit_board_story_path(@board, @story, step_id: @story.step_id),
      title: 'Edit Story',
      remote: true, class: 'edit-story-button btn btn-primary'
    end
  end

  def delete_story_button(story = @story)
    if story.owner == current_user
      link_to board_story_path(@board, story), method: :delete, 
      title: 'Delete Story',
      data: { confirm: 'Are you sure?' },  
      remote: true, class: 'delete-story-button list-actions' do
        content_tag(:span, '', class: 'glyphicon glyphicon-remove')
      end
    end
  end
end

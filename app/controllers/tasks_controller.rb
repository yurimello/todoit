class TasksController < ApplicationController
  before_action :authenticate_user!
  before_filter :set_board
  before_filter :set_story
  before_filter :story_is_editable_for_user?, only: [:new, :edit, :create, :update]


  layout false
  
  def edit
    @task = Task.find(params[:id])
  end
  
  def new
    @task = Task.new
  end

  def create
    @task = Task.new(task_params)

    respond_to do |format|
      if @task.save
        format.html { redirect_to @board, notice: 'Task was successfully created.' }
        format.json { render :show, status: :created, location: @task }
        format.js 
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

   def update
    @task = Task.find(params[:id])
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to @board, notice: 'Task was successfully updated.' }
        format.json { render :show, status: :ok, location: @task }
        format.js 
      else
        format.html { render :edit }
        format.json { render json: @task.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def destroy
    @task = Task.find(params[:id])
    @task_id = @task.id
    respond_to do |format|
      if @task.destroy
        format.html { redirect_to @board, notice: 'Task was successfully destroyed.' }
        format.json { render :show, status: :ok, location: @task }
        format.js
      end
    end
  end

  private
  def set_board
    @board = Board.find(params[:board_id])
  end

  def set_story
    @story = Story.find(params[:story_id])
  end

  def task_params
    params.fetch(:task, [:name, :story_id]).permit!
  end

  def story_is_editable_for_user?
    if @story.owner != current_user
      flash[:notice] = t('.errors_messages.access_denied')
      redirect_to board_path(@board)
    end
  end
end

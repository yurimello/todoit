class StepsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_board

  
  def new
    @step = Step.new
  end

  def edit
    @step = Step.find(params[:id])
  end

  def create
    @step = Step.new(step_params)

    respond_to do |format|
      if @step.save
        format.html { redirect_to @board, notice: 'Step was successfully created.' }
        format.json { render :show, status: :created, location: @step }
        format.js
      else
        format.html { render :new }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

   def update
    @step = Step.find(params[:id])
    respond_to do |format|
      if @step.update(step_params)
        format.html { redirect_to @board, notice: 'Step was successfully updated.' }
        format.json { render :show, status: :ok, location: @step }
      else
        format.html { render :edit }
        format.json { render json: @step.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @step = Step.find(params[:id])
    @step.destroy
    respond_to do |format|
      format.html { redirect_to board_path(@board), notice: 'Step was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_board
      @board = Board.find(params[:board_id])
    end

    def step_params
      params.fetch(:step, [:name, :position, :board_id]).permit!
    end
end

class StoriesController < ApplicationController
  before_action :authenticate_user!
  before_filter :set_board
  before_filter :set_step, only: [:new, :edit]
  before_filter :set_story, only: [:show, :edit, :update, :destroy]
  before_filter :story_is_accessible_for_user?, only: [:show]
  before_filter :story_is_editable_for_user?, only: [:edit, :update]

  layout false

  def show
  end

  def edit
  end
  
  def new
    @story = Story.new
  end

  def create
    @story = Story.new(story_params)
    @story.owner = current_user

    respond_to do |format|
      if @story.save
        format.html { redirect_to @board, notice: 'Story was successfully created.' }
        format.json { render :show, status: :created, location: @story }
        format.js
      else
        @step = @story.step
        format.html { render :new }
        format.json { render json: @story.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def update
    respond_to do |format|
      if @story.update(story_params)
        format.html { redirect_to @board, notice: 'Story was successfully updated.' }
        format.json { render :show, status: :ok, location: @story }
        format.js do
          if params[:sorting].blank?
            render :update
          else
            render text: 'sorted'
          end
        end
      else
        @step = @story.step
        format.html { render :edit }
        format.json { render json: @step.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def destroy
    @story_id = @story.id
    respond_to do |format|
      if @story.destroy
        format.html { redirect_to @board, notice: 'Story was successfully destroyed.' }
        format.json { render :show, status: :ok, location: @story }
        format.js
      end
    end
  end


  private
  def set_story
    @story = Story.find(params[:id])
  end

  def set_board
    @board = Board.find(params[:board_id])
  end

  def set_step
    @step = Step.find(params[:step_id])
  end

  def story_params
    params.fetch(:story, [:name, :position, :step_id, :is_private]).permit!
  end

  def story_is_accessible_for_user?
    if @story.is_private? && @story.owner != current_user
      flash[:notice] = t('.errors_messages.access_denied')
      redirect_to board_path(@board)
    end
  end

  def story_is_editable_for_user?
    if params[:sorting].blank? && @story.owner != current_user
      flash[:notice] = t('.errors_messages.access_denied')
      redirect_to board_path(@board)
    end
  end
end

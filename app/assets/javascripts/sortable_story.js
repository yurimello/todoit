SortableStory = function(evt, ui){
  this.ui = ui
  this.currentEvent = evt

  this.sortable = function(){
    $('.stories-list').sortable({
      connectWith: '.stories-list',
      update: function(event, ui){
        sortable_story = new SortableStory(event, ui)
        sortable_story.sort()
      }
    }).disableSelection();
  }

  this.sort = function(){
    position = this.currentPosition()
    if(position >= 0){
      this.updatePosition(position)
    }    
  }

  this.updatePosition = function(position){
    $.ajax({
      url: this.ui.item.data('update_path'),
      method: 'put',
      dataType: 'script',
      data: {
        sorting: true,
        story: { 
          step_id: this.sortTarget().data('step_id'),
          position: position
        }
      }
    })
  }
  
  this.currentPosition = function(){
    return this.sortTarget().find('li').index(this.ui.item)
  }

  this.sortTarget = function(){
    return $(this.currentEvent.target)
  }

  this.uiItem = function() {
    return this.ui.item
  }
}
Story = function(evt){
  this.currentEvent    = evt
  this.newButtonClass  = '.new-story-button'
  this.editButtonClass = '.edit-story-button'
  this.newFormClass    = '.new-story-form'
  this.editFormClass   = '.edit-story-form'

  this.setBindings = function(){
    this.newButtonBinding()
    this.showButtonBinding()
  }

  this.showButtonBinding = function(){
    $('.story-show-button').bind('ajax:success', function(evt, data, status, xhr){
      story = new Story(evt)
      story.show(data)
    })
  }

  this.editButtonBinding = function(){
    $('.edit-story-button').bind('ajax:success', function(evt, data, status, xhr){
      story = new Story(evt)
      story.showEditForm(data)
    })
  }

  this.newButtonBinding = function(){
    $('.new-story-button').bind('ajax:success', function(evt, data, status, xhr){
      story = new Story(evt)
      story.showNewForm(data)
    })
  }

  this.cancelNewFormButtonBinding = function(){
    $('.cancel-form-story-button').bind('click', function(evt){
      story = new Story(evt)
      story.cancelNewForm()
    })
  }

  this.cancelEditFormButtonBinding = function(){
    $('.cancel-form-story-button').bind('click', function(evt){
      story = new Story(evt)
      story.cancelEditForm()
    })
  }

  this.show = function(data) {
    $('#story-modal').hide()
    $('#story-modal').html(data)
    
    this.editButtonBinding()
    this.setTaskBindings()

    this.showModal()
    
  }

  this.create = function(story_data, step_selector, status){
    this.currentEvent = new FakeEvent($(step_selector).find('.stories-list'))

    if(status == 'error'){
      this.showNewForm(story_data)
    }
    else{
      form = this.newForm()
      form.hide()
      this.newButton().show()
      form.html('')
      this.list().append(story_data)
    }
    this.setBindings()
  }

  this.update = function(story_data, story_selector, story_name, status){
    this.currentEvent = new FakeEvent($(story_selector))

    form = this.editForm()
    
    if(status == 'error'){
      form.html(story_data)
    }
    else{
      $('.page-header').find('h1').html(story_name)
      $('.story').html(story_data)
      $('.show-story').show()
      this.editButton().show()
    }
    
    this.editButtonBinding()
    this.cancelEditFormButtonBinding()
  }

  this.showModal = function(){
    $('#story-modal').dialog({
      modal: true,
      width: 1000,
    })
  }

  this.cancelNewForm = function(){
    form = this.newForm()
    new_button = this.newButton().show()

    form.html('')
    form.hide()
    new_button.show()
  }

  this.cancelEditForm = function(){
    form = this.editForm()
    edit_button = this.editButton()
    form.html('')
    form.hide()
    edit_button.show()
  }

  this.showNewForm = function(data){
    form = this.newForm()
    this.newButton().hide()
    form.html(data)
    form.show()

    this.cancelNewFormButtonBinding()
  }

  this.showEditForm = function(data){
    form = this.editForm()
    
    this.editButton().hide()
    form.html(data)
    form.show()

    this.cancelEditFormButtonBinding()
  }

  this.newButton = function(){
    return this.stepPanel().find(this.newButtonClass)
  }

  this.newForm = function(){
    return this.stepPanel().find(this.newFormClass)
  }

  this.editButton = function(){
    return this.storyModal().find(this.editButtonClass)
  }

  this.editForm = function(){
    return this.storyModal().find(this.editFormClass)
  }

  this.list = function(){
    return this.stepPanel().find('.list-group')
  }

  this.stepPanel = function(){
    return this.currentTarget().parents('.panel')
  }

  this.storyModal = function(){
    return this.currentTarget().parents('#story-modal')
  }

  this.currentTarget = function(){
    return $(this.currentEvent.target)
  }

  this.setTaskBindings = function(){
    task = new Task
    task.setBindings()
  }
 
}
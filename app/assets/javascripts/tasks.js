Task = function(){
  this.setBindings = function(){
    this.setEditFormButtonBinding()
    this.setNewFormButtonBinding()
  }
  
  this.setEditFormButtonBinding = function(){
    $('.edit-task-button').bind('ajax:success', function(evt, data, status, xhr){
      task = new Task
      task.showEditForm(evt, data)
    })
  }
  
  this.setCancelEditFormButtonBinding = function(){
    $('.cancel-edit-task-button').bind('click', function(evt){
      task = new Task
      task.cancelEditForm(evt)
    })
  }

  this.setSaveButtonBinding = function(){
    $('.save-task-button').on('click', function(evt){
      $(this).submit()
    })
  }

  this.setNewFormButtonBinding = function(){
    $('.new-task-button').bind('ajax:success', function(evt, data, status, xhr){
      task = new Task
      task.showNewForm(data)
    })
  }
  
  this.setCancelNewFormButtonBinding = function(){
    $('.cancel-new-task-button').bind('click', function(evt){
      task = new Task
      task.cancelNewForm()
    })
  }

  this.findTaskElement = function(evt) {
    return $(evt.target).parents('.list-group-item')
  }

  this.findTaskListElement = function(evt){
    return $(evt.target).parents('.list-group')
  }

  this.findHiddenTask = function(task_list_element, task_element){
    return task_list_element.find('#hidden_' + task_element.attr('id'))
  }

  this.buildHiddenTask = function(task_element) {
    hidden_html = "<div id='hidden_" + task_element.attr('id') + "'></div>"
    return $(hidden_html).hide()
  }

  this.showNewForm = function(form_data) {
    $('.new-task-button').hide()
    $('#new-task-form').html(form_data)
    $('#new-task-form').show()

    this.setCancelNewFormButtonBinding()
  }

  this.cancelNewForm = function(){
    $('#new-task-form').hide()        
    $('#new-task-form').html('')
    $('.new-task-button').show()
  }

  this.cancelEditForm = function(evt){
    task_element = this.findTaskElement(evt)
    task_list_element = this.findTaskListElement(evt)
    hidden_task = this.findHiddenTask(task_list_element, task_element)
    
    task_element.html(hidden_task.children())
    hidden_task.remove()
  }

  this.showEditForm = function(evt, form_data) {
    task_element = this.findTaskElement(evt)
    task_list_element = this.findTaskListElement(evt)

    hidden_task = this.buildHiddenTask(task_element)
    
    hidden_task.append(task_element.children())

    task_list_element.append(hidden_task)
    
    task_element.html(form_data)
    this.setCancelEditFormButtonBinding()
    this.setSaveButtonBinding()
  }

  this.create = function(task_data, status){
    if(status == 'error'){
      this.showNewForm(task_data)
    }
    else{
      form = $('#new-task-form')
      form.hide()
      form.html('')
      $('.new-task-button').show()
      $('#tasks-list').append(task_data)
    }
    this.setBindings()
  }

  this.update = function(task_element, task_data){
    task_element.html(task_data)
    
    this.setEditFormButtonBinding()
    this.setCancelEditFormButtonBinding()
  }
}
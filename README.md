#Todoit

Simple Task Manager for Avenue Code Challenge

##Ruby version
2.2.4

##Database creation
`rake db:create`

`rake db:migrate`

##Database initialization
`rake db:seed`

##How to run the test suite
`rake spec`

##Runinng on
[http://todoit-challenge.herokuapp.com/](http://todoit-challenge.herokuapp.com/)

###Login
email: *user@email.com*

password: *password123*
Rails.application.routes.draw do

  resources :boards do

    resources :stories do
      resources :tasks
    end

    resources :steps
  end

  devise_for :users

  root to: 'boards#index'
end
